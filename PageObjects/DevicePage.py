from Locators.DeviceLocators import DeviceLocators


class BasePage(object):

    def __init__(self, driver):
        self.driver = driver


class DevicesPage(BasePage):

    def title_is_visible(self):
        element = self.driver.find_element(*DeviceLocators.TITLE_3)
        if element:
            return True

    def click_next_3page(self):
        element = self.driver.find_element(*DeviceLocators.NEXT_3_BTN)
        element.click()
