from Locators.HomeLocators import HomeLocators


class BasePage(object):

    def __init__(self, driver):
        self.driver = driver


class MainPage(BasePage):

    def click_signin(self):
        element = self.driver.find_element(*HomeLocators.SIGN_UP_FREE)
        element.click()

    def result_is_visible(self):
        element = self.driver.find_element(*HomeLocators.HOME_CHECK)
        if element:
            return True