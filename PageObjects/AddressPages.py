import time
from Locators.AddressLocators import AddressLocators
from selenium.webdriver.common.keys import Keys

class BasePage(object):

    def __init__(self, driver):
        self.driver = driver


class AddressPage(BasePage):

    def title_is_visible(self):
        element = self.driver.find_element(*AddressLocators.TITLE_2)
        if element:
            return True

    def set_city(self):
        element = self.driver.find_element(*AddressLocators.CITY_NAME)
        element.send_keys("Bangkok")

    def click_city(self):
        element = self.driver.find_element(*AddressLocators.CITY_CLICK)
        element.click()

    def click_zipcode(self):
        element = self.driver.find_element(*AddressLocators.ZIPCODE)
        element.click()

    def set_zipcode(self):
        time.sleep(5)
        element = self.driver.find_element(*AddressLocators.ZIPCODE)
        element.send_keys("10300")
        element.send_keys(Keys.TAB)

    def click_next_3page(self):
        element = self.driver.find_element(*AddressLocators.NEXT_2_BTN)
        element.click()