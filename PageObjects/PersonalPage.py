from Locators.PersonalLocators import PersonalLocators
from selenium.webdriver.common.keys import Keys


class BasePage(object):

    def __init__(self, driver):
        self.driver = driver


class PersonalPage(BasePage):

    def title_is_visible(self):
        element = self.driver.find_element(*PersonalLocators.TITLE_1)
        if element:
            return True

    def set_firstname(self):
        element = self.driver.find_element(*PersonalLocators.FIRST_NAME)
        element.send_keys("Daniel")

    def set_lastname(self):
        element = self.driver.find_element(*PersonalLocators.LAST_NAME)
        element.send_keys("Wellington")

    def set_email(self):
        element = self.driver.find_element(*PersonalLocators.EMAIL)
        element.send_keys("daniel@yopmail.com")

    def click_gender(self):
        element = self.driver.find_element(*PersonalLocators.GENDER)
        element.click()

    def set_gender(self):
        element = self.driver.find_element(*PersonalLocators.GENDER_TXT)
        element.send_keys("Male")
        element.send_keys(Keys.ENTER)

    def click_birthmonth(self):
        element = self.driver.find_element(*PersonalLocators.BIRTH_MONTH)
        element.click()

    def set_birthmonth(self):
        element = self.driver.find_element(*PersonalLocators.MONTH_TXT)
        element.send_keys("January")
        element.send_keys(Keys.ENTER)

    def click_birthday(self):
        element = self.driver.find_element(*PersonalLocators.BIRTH_DAY)
        element.click()

    def set_birthday(self):
        element = self.driver.find_element(*PersonalLocators.DAY_TXT)
        element.send_keys("10")
        element.send_keys(Keys.ENTER)

    def click_birthyear(self):
        element = self.driver.find_element(*PersonalLocators.BIRTH_YEAR)
        element.click()

    def set_birthyear(self):
        element = self.driver.find_element(*PersonalLocators.YEAR_TXT)
        element.send_keys("1991")
        element.send_keys(Keys.ENTER)

    def click_next_2page(self):
        element = self.driver.find_element(*PersonalLocators.NEXT_1_BTN)
        element.click()