from Locators.CompleteLocators import CompleteLocators
import time
from selenium.webdriver.common.keys import Keys

class BasePage(object):

    def __init__(self, driver):
        self.driver = driver


class CompletePage(BasePage):

    def title_is_visible(self):
        element = self.driver.find_element(*CompleteLocators.TITLE_4)
        if element:
            return True

    def set_password(self):
        time.sleep(5)
        element = self.driver.find_element(*CompleteLocators.PASSWORD)
        element.send_keys("D@nail123456789")
        #element.send_keys(Keys.TAB)

    def click_cf_password(self):
        element = self.driver.find_element(*CompleteLocators.CF_PASSWORD)
        element.click()

    def set_cf_password(self):
        time.sleep(5)
        element = self.driver.find_element(*CompleteLocators.CF_PASSWORD)
        element.send_keys("D@nail123456789")
        #element.send_keys(Keys.TAB)

    def click_check_tc(self):
        element = self.driver.find_element(*CompleteLocators.CHECK_TC)
        element.click()

    def click_check_policy(self):
        element = self.driver.find_element(*CompleteLocators.CHECK_POLICY)
        element.click()

    def click_next_4page(self):
        element = self.driver.find_element(*CompleteLocators.NEXT_4_BTN)
        element.click()