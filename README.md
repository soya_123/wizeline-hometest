### Configuration

#### 1. Ensure your machine have python installed

#### 2. Install Selenium Python
If you have pip on your system, you can simply install or upgrade the Python bindings:
```javascript
pip install -U selenium
```

Alternately, you can download the source distribution from PyPI (e.g. selenium-4.0.0a1.tar.gz), unarchive it, and run:
```javascript
python setup.py install
```


### How to run?
Issue the below commands in project root directory

```javascript
python -m unittest TestCases.uTest
```
By default it runs in Chrome browser, you can specify which browser to use as well
```javascript
python -m unittest TestCases.uTest --browser=chrome
```

Currently supported browsers are
* chrome
* firefox
* edge
* ie

> Feel free to modify it to your own needs :)