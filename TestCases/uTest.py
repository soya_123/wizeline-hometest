import unittest
from PageObjects import HomePage
from PageObjects import PersonalPage
from PageObjects import AddressPages
from PageObjects import DevicePage
from PageObjects import CompletePage
from selenium import webdriver


class SignUpTest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(10)
        self.driver.get('https://www.utest.com')

    def test_happy_signup_for_free(self):
        home_page = HomePage.MainPage(self.driver)
        home_page.click_signin()

        personal_page = PersonalPage.PersonalPage(self.driver)
        assert personal_page.title_is_visible()
        personal_page.set_firstname()
        personal_page.set_lastname()
        personal_page.set_email()
        personal_page.click_gender()
        personal_page.set_gender()
        personal_page.click_birthmonth()
        personal_page.set_birthmonth()
        personal_page.click_birthday()
        personal_page.set_birthday()
        personal_page.click_birthyear()
        personal_page.set_birthyear()
        personal_page.click_next_2page()

        address_page = AddressPages.AddressPage(self.driver)
        assert address_page.title_is_visible()
        address_page.set_city()
        address_page.click_city()
        # TODO enhance this line when have more time
        address_page.click_zipcode()
        address_page.set_zipcode()
        address_page.click_next_3page()

        device_page = DevicePage.DevicesPage(self.driver)
        assert device_page.title_is_visible()
        device_page.click_next_3page()

        complete_page = CompletePage.CompletePage(self.driver)
        assert complete_page.title_is_visible()

        # TODO enhance this line when have more time
        complete_page.set_password()
        complete_page.click_cf_password()
        complete_page.set_cf_password()
        complete_page.click_check_tc()
        complete_page.click_check_policy()
        complete_page.click_next_4page()

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()