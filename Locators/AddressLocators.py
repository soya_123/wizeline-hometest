from selenium.webdriver.common.by import By

class AddressLocators(object):
    TITLE_2 = (By.XPATH, '//*[@id="regs_container"]/div/div[2]/div/div[1]/div/div/h1/span[contains(.,"Add your address")]')
    CITY_NAME = (By.NAME, 'city')
    CITY_CLICK = (By.XPATH, '//div[contains(@class,"pac-container")]/div[contains(@class, "pac-item")]/span[contains(@class, "pac-item-query")]/span[contains(.,"Bangkok")]')
    ZIPCODE = (By.NAME, 'zip')
    NEXT_2_BTN = (By.XPATH, '//*[@id="regs_container"]/div/div[2]/div/div[2]/div/form/div[2]/div/a[contains(@class,"btn-blue")]')