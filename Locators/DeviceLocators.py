from selenium.webdriver.common.by import By

class DeviceLocators(object):
    TITLE_3 = (By.XPATH, '//*[@id="regs_container"]/div/div[2]/div/div[1]/div/div/h1/span[contains(.,"Tell us about your devices")]')
    NEXT_3_BTN = (By.XPATH, '//*[@id="regs_container"]/div/div[2]/div/div[2]/div/div[2]/div/a[contains(@class,"btn-blue")]')