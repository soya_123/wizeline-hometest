from selenium.webdriver.common.by import By

class HomeLocators(object):
    SIGN_UP_FREE = (By.XPATH, '//*[@id="nav-bar-container"]/div/div/div/a[contains(.,"Sign Up For Free")]')
    HOME_CHECK = (By.XPATH, '//div/nav/div[2]/nav-sidebar-item[4]/div/a/div[contains(.,"Projects Board")]')