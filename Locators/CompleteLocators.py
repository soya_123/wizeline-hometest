from selenium.webdriver.common.by import By

class CompleteLocators(object):
    TITLE_4 = (By.XPATH, '//*[@id="regs_container"]/div/div[2]/div/div[1]/div/div/h1/span[contains(.,"The last step")]')
    PASSWORD = (By.NAME, 'password')
    CF_PASSWORD = (By.NAME, 'confirmPassword')
    CHECK_TC = (By.XPATH, '//input[@name="termOfUse"]/following-sibling::span')
    CHECK_POLICY = (By.XPATH, '//input[@name="privacySetting"]/following-sibling::span')
    NEXT_4_BTN = (By.XPATH, '//div/a[contains(@class,"btn-blue") and @id="laddaBtn"]')