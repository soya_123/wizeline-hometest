from selenium.webdriver.common.by import By

class PersonalLocators(object):
    TITLE_1 = (By.XPATH, '//*[@id="regs_container"]/div/div[2]/div/div[1]/div/div/h1/span[contains(.,"Tell us about")]')
    FIRST_NAME = (By.NAME, 'firstName')
    LAST_NAME = (By.NAME, 'lastName')
    EMAIL = (By.NAME, 'email')
    GENDER = (By.NAME, 'genderCode')
    GENDER_TXT = (By.XPATH, '//*[@id="genderCode"]/input[@type="search"]')
    BIRTH_MONTH = (By.NAME, 'birthMonth')
    MONTH_TXT = (By.XPATH, '//*[@id="birthDate"]/div[1]/div/input[@type="search"]')
    BIRTH_DAY = (By.NAME, 'birthDay')
    DAY_TXT = (By.XPATH, '//*[@id="birthDate"]/div[2]/div/input[@type="search"]')
    BIRTH_YEAR = (By.NAME, 'birthYear')
    YEAR_TXT = (By.XPATH, '//*[@id="birthDate"]/div[3]/div/input[@type="search"]')
    GENDER = (By.NAME, 'genderCode')
    GENDER_TXT = (By.XPATH, '//*[@id="genderCode"]/input[@type="search"]')
    NEXT_1_BTN = (By.XPATH, '//*[@id="regs_container"]/div/div[2]/div/div[2]/div/form/div[2]/a[contains(@class,"btn")]')