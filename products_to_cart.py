from selenium import webdriver
from selenium.webdriver.common.keys import Keys


browser = webdriver.Chrome()
browser.get('https://www.saucedemo.com/index.html')

#variables
userName = 'standard_user'
passWord = 'secret_sauce'
priceProduct = '29.99'

#login with valid username and password
userName_element = browser.find_element_by_id('user-name')
userName_element.send_keys(userName)
passWord_element = browser.find_element_by_id('password')
passWord_element.send_keys(passWord)
passWord_element.send_keys(Keys.ENTER)

#Add product to cart
productTitle_element = browser.find_element_by_xpath('//div[@class="product_label"]').text
assert productTitle_element, "Products"
addToCart_29_button = browser.find_element_by_xpath('//div[@class="inventory_item"]//div[@class="inventory_item_price" and contains(., priceProduct)]/following-sibling::button')
addToCart_29_button.click()

#Check amout of item in your cart
cartBadge_element = browser.find_element_by_xpath('//*[@id="shopping_cart_container"]/a/span').text
assert cartBadge_element, 1

#Verify item in cart
cart_element = browser.find_element_by_id("shopping_cart_container")
cart_element.click()

cartTitle_element = browser.find_element_by_xpath('//*[@id="contents_wrapper"]/div[2]').text
assert cartTitle_element, "Your Cart"

itemInCart_element = browser.find_element_by_xpath('//*[@id="cart_contents_container"]/div/div[1]/div[3]/div[2]/div[2]/div').text
assert itemInCart_element, priceProduct

browser.quit()

